# NachOS assignments: File system


## Goal

1. **Modify the file system code to support file I/O system call and larger file size**
    - Combine your MP1 file system call interface with NachOS FS  
    For your implementation simplicity, you may assume that all test cases do not contain
    any messy operations. (E.g.create a file that already exists, read/write exceeds the file
    size, etc.)
    - Implement five system calls:  
        - `int Create(char *name, int size)`  
        Create a file with the name and with size bytes in the root directory. The character in
        name only contains [A-Za-z0-9.] and with length not greater than 9. Here, this
        operation will always succeed and return 1
        - `OpenFileId Open(char *name)`  
        Open the file with `name` and return its `OpenFileId`. **Only at most one file will be
        opened at the same time**. Here, any `OpenFileId` larger than 0 is considered as a
        successful open. (You do not need to maintain OpenFileTable in this assignment)
        - `int Read(char *buf, int size, OpenFileId id)`  
        - `int Write(char *buf, int size, OpenFileId id)`  
        Read/Write size characters from/to the file to/from `buf`. Return number of characters
        actually read/written from/to the file. Here, id will always be valid and no messy
        operations will be given.
        - `int Close(OpenFileId id)`  
        Close the file by id. Here, this operation will always succeed and return 1.
    - Enhance the FS to let it support up to 32KB file size  
    You can use any approach including modify the allocation scheme or extend the data block pointer structure, etc.  
    For your implementation simplicity, you may assume that all of the operations will not be messy. (E.g. copy a file larger than 32KB, try to print a non-existing file, etc.)

2. **Modify the file system code to support subdirectory**  
    For your implementation simplicity, you may assume that all the operations will not be messy. (E.g. remove a non-existing file, copy a file into a non-existing directory, create a directory in a non-existing directory, list a file instead of a directory, etc.)
    - Implement the subdirectory structure
        - Use ‘/’ as path name separator
        - Path has maximum length of 255
        - Length of directory and file name does not exceed 9.
        - All paths are absolute (e.g. /testing/num100, /1000, etc.)
    - Support up to 64 files/subdirectories per directory

3. **Bonus Assignment**
    - Bonus I: Enhance the NachOS to support even larger file size
        - Extend the disk from 128KB to 64MB
        - Support up to 64 MB single file
    - Bonus II: Multi-level header size
        - Show that smaller file can have smaller header size
        - Implement at least 3 different size of headers for different size of files
        - Design your own test cases to show your implementation is correct.
    - Bonus III. Recursive Operations on Directories
        - Support recursive remove of a directory


**Please refer to the [report](./MP4_report.pdf) for more details.**

## Appendix: What is NachOS?

> *NachOS* is instructional software for teaching undergraduate, and potentially graduate, level operating systems courses.  
> Website: https://homes.cs.washington.edu/~tom/nachos/
