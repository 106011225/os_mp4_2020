/**************************************************************
 *
 * userprog/ksyscall.h
 *
 * Kernel interface for systemcalls 
 *
 * by Marcus Voelp  (c) Universitaet Karlsruhe
 *
 **************************************************************/

#ifndef __USERPROG_KSYSCALL_H__
#define __USERPROG_KSYSCALL_H__

#include "kernel.h"

#include "synchconsole.h"

void SysHalt()
{
	kernel->interrupt->Halt();
}

int SysAdd(int op1, int op2)
{
	return op1 + op2;
}

#ifdef FILESYS_STUB
int SysCreate(char *filename)
{
	// return value
	// 1: success
	// 0: failed
	return kernel->interrupt->CreateFile(filename);
    /* comment by liuchinlin @ mp4 */
    // this func return "kernel->CreateFile(filename)"
    // and this func return "fileSystem->Create(filename)"
    /* /comment by liuchinlin @ mp4 */
}

/* added by liuchinlin @ mp4 */
#else
int SysCreate(char *filename, int initialSize)
{
	// return value
	// 1: success
	// 0: failed
	return kernel->fileSystem->Create(filename, initialSize);
}
/* /added by liuchinlin @ mp4 */
#endif /* FILESYS_STUB */

/* add by liuchinlin @ mp4 */

int SysOpenAFile(char *name){
    //DEBUG(dbgTraCode, "In ksyscall.h:SysOpenAFile, into fileSystem->OpenAFile, " << kernel->stats->totalTicks);
    //return 1 for success, -1 for fail
    return kernel->fileSystem->OpenAFile(name);
}

int SysWriteFile(char *buffer, int size, int id){
    //return -1 for fail, otherwise return # of size written successfully
    return kernel->fileSystem->WriteFile(buffer, size, id);
}

int SysReadFile(char *buffer, int size, int id){
    //return -1 for fail, otherwise return # of size read successfully
    return kernel->fileSystem->ReadFile(buffer, size, id);
}

int SysCloseFile(int id){
    //return 1 for success, -1 for fail
    return kernel->fileSystem->CloseFile(id);
}

/* /added by liuchinlin @ mp4 */ 

#endif /* ! __USERPROG_KSYSCALL_H__ */
