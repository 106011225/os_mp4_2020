// directory.cc
//	Routines to manage a directory of file names.
//
//	The directory is a table of fixed length entries; each
//	entry represents a single file, and contains the file name,
//	and the location of the file header on disk.  The fixed size
//	of each directory entry means that we have the restriction
//	of a fixed maximum size for file names.
//
//	The constructor initializes an empty directory of a certain size;
//	we use ReadFrom/WriteBack to fetch the contents of the directory
//	from disk, and to write back any modifications back to disk.
//
//	Also, this implementation has the restriction that the size
//	of the directory cannot expand.  In other words, once all the
//	entries in the directory are used, no more files can be created.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "utility.h"
#include "filehdr.h"
#include "directory.h"

//----------------------------------------------------------------------
// Directory::Directory
// 	Initialize a directory; initially, the directory is completely
//	empty.  If the disk is being formatted, an empty directory
//	is all we need, but otherwise, we need to call FetchFrom in order
//	to initialize it from disk.
//
//	"size" is the number of entries in the directory
//----------------------------------------------------------------------

Directory::Directory(int size)
{
    table = new DirectoryEntry[size];

    // MP4 mod tag
    memset(table, 0, sizeof(DirectoryEntry) * size); // dummy operation to keep valgrind happy

    tableSize = size;

    /* modify by liuchinlin @ mp4 */
    // for (int i = 0; i < tableSize; i++)
    //     table[i].inUse = FALSE;

    for (int i = 0; i < tableSize; i++){
        table[i].inUse = FALSE;
        table[i].sector = -1;
        table[i].child = -1;
        table[i].sibling = -1;
    }

    // use table[0] to store root/
    table[0].inUse = TRUE;
    table[0].isFolder = TRUE;
    strcpy(table[0].name, "root");
    /* /modify by liuchinlin @ mp4 */
}

//----------------------------------------------------------------------
// Directory::~Directory
// 	De-allocate directory data structure.
//----------------------------------------------------------------------

Directory::~Directory()
{
    delete[] table;
}

//----------------------------------------------------------------------
// Directory::FetchFrom
// 	Read the contents of the directory from disk.
//
//	"file" -- file containing the directory contents
//----------------------------------------------------------------------

void Directory::FetchFrom(OpenFile *file)
{
    (void)file->ReadAt((char *)table, tableSize * sizeof(DirectoryEntry), 0);
}

//----------------------------------------------------------------------
// Directory::WriteBack
// 	Write any modifications to the directory back to disk
//
//	"file" -- file to contain the new directory contents
//----------------------------------------------------------------------

void Directory::WriteBack(OpenFile *file)
{
    (void)file->WriteAt((char *)table, tableSize * sizeof(DirectoryEntry), 0);
}

//----------------------------------------------------------------------
// Directory::FindIndex
// 	Look up file name in directory, and return its location in the table of
//	directory entries.  Return -1 if the name isn't in the directory.
//
//	"name" -- the file name to look up
//----------------------------------------------------------------------

/* comment out by liuchinlin @ mp4 */
// int Directory::FindIndex(char *name)
// {
//     for (int i = 0; i < tableSize; i++)
//         if (table[i].inUse && !strncmp(table[i].name, name, FileNameMaxLen))
//             return i;
//     return -1; // name not in directory
// }
/* /comment out by liuchinlin @ mp4 */

//----------------------------------------------------------------------
// Directory::Find
// 	Look up file name in directory, and return the disk sector number
//	where the file's header is stored. Return -1 if the name isn't
//	in the directory.
//
//	"name" -- the file name to look up
//----------------------------------------------------------------------

/* modify by liuchinlin @ mp4 */
int Directory::Find(char *name)
{
    // int i = FindIndex(name);

    // if (i != -1)
    //     return table[i].sector;
    // return -1;

    // assume name = "/abc/def/ghi"
    char fileName[FileNameMaxLen + 1];//store file name, it can also be a folder we want to create
    char path[PathMaxLen + 1];// store path w/o file name, separating with ' '
    SetPathAndName(name, path, fileName);
    // then fileName = "ghi"
    //      path = " abc def"
    
    int targetFolder = FindTargetFolder(path);        
    if (targetFolder == -1) return -1; // not found
    int nodeIndex = FindUnderFolder(fileName, targetFolder);
    if (nodeIndex == -1) return -1; // not found
    //ASSERT((nodeIndex >= 0) && (table[nodeIndex].isFolder == FALSE));

    
    return table[nodeIndex].sector;
}
/* /modify by liuchinlin @ mp4 */


/* add by liuchinlin @ mp4 */

void
Directory::SetPathAndName(char *str, char *path, char *fileName){
    // assume str = "/abc/def/ghi"
    // then fileName = "ghi"
    //      path = " abc def"

    memset(fileName, 0, sizeof(char)*(FileNameMaxLen + 1));
    memset(path, 0, sizeof(char)*(PathMaxLen + 1));
    strcpy(path,str);
    strrev(path);
    for (int i=0; i<strlen(path); i++)
        if (path[i] == '/') path[i] = ' ';  
    sscanf(path, "%s", fileName);
    strrev(fileName);
    strrev(path);
    path[strlen(path) - strlen(fileName) - 1] = '\0';
}

void strrev(char *str){
    for (int i=0; i<strlen(str)/2; i++)
        std::swap(str[i], str[strlen(str) - 1 - i]);
}

int 
Directory::FindTargetFolder(char *path){
    // path = " abc def"...
    int targetFolder = 0;// search starting from root
    char folderName[FileNameMaxLen + 1];
        
    while(strlen(path) > 1) { // min = " ", lenth=1
        sscanf(path, "%s", folderName);
        path += (strlen(folderName) + 1)*sizeof(char);
        targetFolder = FindUnderFolder(folderName, targetFolder);
        if (targetFolder == -1) return -1; // not found
    }
    return targetFolder;
}

bool 
Directory::AddUnderFolder(char *name, int newSector, bool isFolder, int targetFolder){
    
    //find empty table entry and set it
    int newEntryIndex;
    for (newEntryIndex = 0; newEntryIndex < tableSize; newEntryIndex++){
        if (!table[newEntryIndex].inUse) break;
    }
    if (newEntryIndex == tableSize)return FALSE;	// no space.  Fix when we have extensible files.
    
    table[newEntryIndex].inUse = TRUE;
    table[newEntryIndex].sector = newSector;//if this is a folder, it'll be -1
    table[newEntryIndex].isFolder = isFolder;
    strncpy(table[newEntryIndex].name, name, FileNameMaxLen); 
    
    
    // link to target folder
    int sibling = table[targetFolder].child;
    table[targetFolder].child = newEntryIndex;
    table[newEntryIndex].sibling = sibling;

    return TRUE;
}

int
Directory::FindUnderFolder(char *name, int targetFolder){
    int nodeIndex = table[targetFolder].child;
    while ((nodeIndex != -1) && ( strcmp( table[nodeIndex].name , name ) ))
        nodeIndex = table[nodeIndex].sibling;
    
    return nodeIndex; // if not found, nodeIndex = -1
}

bool
Directory::RemoveUnderFolder(char *name, int targetFolder){

    int current = table[targetFolder].child;

    if ((current != -1) && ( strcmp( table[current].name , name ) == 0 )){ // first child is removing target
        table[targetFolder].child = table[current].sibling;
        table[current].inUse = FALSE;
    }
    else {
        int prev;
        while ((current != -1) && ( strcmp( table[current].name , name ) != 0 )){
            prev = current;
            current = table[current].sibling;
        }
        table[prev].sibling = table[current].sibling;
        table[current].inUse = FALSE;
    }
        
    return TRUE;//assume always success
}

void 
Directory::ListRecursive(char *name, int indent){

    //find target folder
    char path[PathMaxLen + 1];
    memset(path, 0, sizeof(path));
    strcpy(path, name);
    for(int i=0; i<strlen(path); i++)
        if (path[i] == '/') path[i] = ' ';
    int targetFolder = FindTargetFolder(path);
   


    // print file inside target folder
    int current = table[targetFolder].child;
    while(current != -1){
        printf("%*s (%s)\n", (indent + strlen(table[current].name)) , table[current].name, (table[current].isFolder ? "D" : "F"));
        
        if (table[current].isFolder){
            char folderPath[PathMaxLen];
            memset(folderPath, 0, sizeof(folderPath));
            if (strlen(name) != 1) // != "/"
                strcpy(folderPath, name);
            
            strcat(folderPath, "/");
            strcat(folderPath, table[current].name);
            ListRecursive(folderPath, indent + 4);
        }
        
        current = table[current].sibling;
    }
}
/* /add by liuchinlin @ mp4 */

//----------------------------------------------------------------------
// Directory::Add
// 	Add a file into the directory.  Return TRUE if successful;
//	return FALSE if the file name is already in the directory, or if
//	the directory is completely full, and has no more space for
//	additional file names.
//
//	"name" -- the name of the file being added
//	"newSector" -- the disk sector containing the added file's header
//----------------------------------------------------------------------

/* modify by liuchinlin @ mp4 */
// bool Directory::Add(char *name, int newSector)
bool Directory::Add(char *name, int newSector, bool isFolder)
{
    // check if the file already exist
    // if (FindIndex(name) != -1)
    //     return FALSE;

    // for (int i = 0; i < tableSize; i++)
    //     if (!table[i].inUse)
    //     {
    //         table[i].inUse = TRUE;
    //         strncpy(table[i].name, name, FileNameMaxLen);
    //         table[i].sector = newSector;
    //         return TRUE;
    //     }
    // return FALSE; // no space.  Fix when we have extensible files.

    // assume name = "/abc/def/ghi"
    char fileName[FileNameMaxLen + 1];//store file name, it can also be a folder we want to create
    char path[PathMaxLen + 1];// store path w/o file name, separating with ' '
    SetPathAndName(name, path, fileName);
    // then fileName = "ghi"
    //      path = " abc def"

    //create new file/folder
    int targetFolder = FindTargetFolder(path);        
    AddUnderFolder(fileName, newSector, isFolder, targetFolder);
            
    return TRUE;
}
/* /modify by liuchinlin @ mp4 */

//----------------------------------------------------------------------
// Directory::Remove
// 	Remove a file name from the directory.  Return TRUE if successful;
//	return FALSE if the file isn't in the directory.
//
//	"name" -- the file name to be removed
//----------------------------------------------------------------------

/* modify be liuchinlin @ mp4 */
bool Directory::Remove(char *name)
{
    // int i = FindIndex(name);

    // if (i == -1)
    //     return FALSE; // name not in directory
    // table[i].inUse = FALSE;
    
    char fileName[FileNameMaxLen + 1];//store file name, it can also be a folder we want to create
    char path[PathMaxLen + 1];// store path w/o file name, separating with ' '
    SetPathAndName(name, path, fileName);

    int targetFolder = FindTargetFolder(path);        
    RemoveUnderFolder(fileName, targetFolder); 

    return TRUE;
}
/* /modify be liuchinlin @ mp4 */

//----------------------------------------------------------------------
// Directory::List
// 	List all the file names in the directory.
//----------------------------------------------------------------------

/* modify by liuchinlin @ mp4 */
// void Directory::List()
void Directory::List(char *name)
{
    // for (int i = 0; i < tableSize; i++)
    //     if (table[i].inUse)
    //         printf("%s\n", table[i].name);
    
  //find target folder
    char path[PathMaxLen + 1];
    memset(path, 0, sizeof(path));
    strcpy(path, name);
    for(int i=0; i<strlen(path); i++)
        if (path[i] == '/') path[i] = ' ';
    int targetFolder = FindTargetFolder(path);

    // print file inside target folder
    int current = table[targetFolder].child;
    while(current != -1){
        printf("%s (%s)\n", table[current].name, (table[current].isFolder ? "D" : "F"));
        current = table[current].sibling;
    }
}
/* /modify by liuchinlin @ mp4 */

/* add by liuchinlin @ mp4 */
void Directory::RList(char *name){
    ListRecursive(name, 0); // indent = 0 for first layer
}
/* /add by liuchinlin @ mp4 */

//----------------------------------------------------------------------
// Directory::Print
// 	List all the file names in the directory, their FileHeader locations,
//	and the contents of each file.  For debugging.
//----------------------------------------------------------------------

void Directory::Print()
{
    /* comment out by liuchinlin @ mp4 */
    // FileHeader *hdr = new FileHeader;

    // printf("Directory contents:\n");
    // for (int i = 0; i < tableSize; i++)
    //     if (table[i].inUse)
    //     {
    //         printf("Name: %s, Sector: %d\n", table[i].name, table[i].sector);
    //         hdr->FetchFrom(table[i].sector);
    //         hdr->Print();
    //     }
    // printf("\n");
    // delete hdr;
    /* /comment out by liuchinlin @ mp4 */
}
