// filehdr.cc
//	Routines for managing the disk file header (in UNIX, this
//	would be called the i-node).
//
//	The file header is used to locate where on disk the
//	file's data is stored.  We implement this as a fixed size
//	table of pointers -- each entry in the table points to the
//	disk sector containing that portion of the file data
//	(in other words, there are no indirect or doubly indirect
//	blocks). The table size is chosen so that the file header
//	will be just big enough to fit in one disk sector,
//
//      Unlike in a real system, we do not keep track of file permissions,
//	ownership, last modification date, etc., in the file header.
//
//	A file header can be initialized in two ways:
//	   for a new file, by modifying the in-memory data structure
//	     to point to the newly allocated data blocks
//	   for a file already on disk, by reading the file header from disk
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation
// of liability and disclaimer of warranty provisions.

#include "copyright.h"

#include "filehdr.h"
#include "debug.h"
#include "synchdisk.h"
#include "main.h"

//----------------------------------------------------------------------
// MP4 mod tag
// FileHeader::FileHeader
//	There is no need to initialize a fileheader,
//	since all the information should be initialized by Allocate or FetchFrom.
//	The purpose of this function is to keep valgrind happy.
//----------------------------------------------------------------------
FileHeader::FileHeader()
{
	numBytes = -1;
	numSectors = -1;

    /* modify by liuchinlin @ mp4 */
    headerSize = SectorSize;
    allocateLevel = -1;
	memset(dataSectorTable, -1, sizeof(dataSectorTable));
	// memset(dataSectors, -1, sizeof(dataSectors));
    /* /modify by liuchinlin @ mp4 */
}

//----------------------------------------------------------------------
// MP4 mod tag
// FileHeader::~FileHeader
//	Currently, there is not need to do anything in destructor function.
//	However, if you decide to add some "in-core" data in header
//	Always remember to deallocate their space or you will leak memory
//----------------------------------------------------------------------
FileHeader::~FileHeader()
{
	// nothing to do now
}

//----------------------------------------------------------------------
// FileHeader::Allocate
// 	Initialize a fresh file header for a newly created file.
//	Allocate data blocks for the file out of the map of free disk blocks.
//	Return FALSE if there are not enough free blocks to accomodate
//	the new file.
//
//	"freeMap" is the bit map of free disk sectors
//	"fileSize" is the bit map of free disk sectors
//----------------------------------------------------------------------

/* modify by liuchinlin @ mp4 */
bool FileHeader::Allocate(PersistentBitmap *freeMap, int fileSize)
{
	numBytes = fileSize;
	numSectors = divRoundUp(fileSize, SectorSize);

    DEBUG(dbgFile, "in FileHeader::Allocate(...), real data size = " << numSectors <<" sectors");

    /* add by liuchinlin @ mp4 */
    DEBUG(dbgMP4bonusII, " file size: " << numBytes << " bytes ");
    /* /add by liuchinlin @ mp4 */

	if (freeMap->NumClear() < numSectors)
		return FALSE; // not enough space

	// for (int i = 0; i < numSectors; i++)
	// {
	//     dataSectors[i] = freeMap->FindAndSet();
	//     // since we checked that there was enough free space,
	//     // we expect this to succeed
	//     ASSERT(dataSectors[i] >= 0);
	// }


    int pointerPerSector = SectorSize / sizeof(int);

    // determine level of allocating, direct, single indirect...
    if (numSectors <= MaxNumDirectSector){
        //direct
        allocateLevel = 0;
        DEBUG(dbgFile, "allocateLevel = " << allocateLevel);

        for (int i = 0; i < numSectors; i++) {
	        dataSectorTable[i] = freeMap->FindAndSet();
	        // since we checked that there was enough free space,
	        // we expect this to succeed
	        ASSERT(dataSectorTable[i] >= 0);
        }

    }
    else if (numSectors <= (pointerPerSector)*MaxNumDirectSector){
        //single indirect
        allocateLevel = 1;
        DEBUG(dbgFile, "allocateLevel = " << allocateLevel);
        
        //int numLvl1Sector = divRoundUp( numSectors , pointerPerSector );
        int realDataSectorCounter = 0; //conut # of data sectors that have been allocated, sectors for locating other sectors are not counted

        memset(dataSectorTable, -1, sizeof(dataSectorTable));//init 
        for(int i=0; i<MaxNumDirectSector; i++){
            //allocate level 1 Sector
	        dataSectorTable[i] = freeMap->FindAndSet();
	        ASSERT(dataSectorTable[i] >= 0);
            headerSize += SectorSize;

            char bufx[SectorSize];
            memset(bufx, -1, sizeof(bufx)); // init bufx
            
            //set the content of a sector which stores locations of other sectors
            for(int offsetx=0; offsetx<SectorSize; offsetx+=sizeof(int)){
                //we only need to allocate new sector if not enough data sectors yet
                if (realDataSectorCounter < numSectors){
                    realDataSectorCounter++;
                    int tmpx = freeMap->FindAndSet();
                    ASSERT(tmpx >= 0);
                    memcpy( (bufx + offsetx) , &tmpx , sizeof(int) );
                }
                if (realDataSectorCounter >= numSectors) break;
            }
            // copy the content into that sector
            kernel->synchDisk->WriteSector( dataSectorTable[i] , bufx );
            if (realDataSectorCounter >= numSectors) DEBUG(dbgFile, "numLvl1Sector = " << i + 1);
            if (realDataSectorCounter >= numSectors) break;
        }
    }
    else if (numSectors <= (pointerPerSector)*(pointerPerSector)*MaxNumDirectSector){
        //double indirect
        allocateLevel = 2;
        DEBUG(dbgFile, "allocateLevel = " << allocateLevel);

        int dbglvl2cnt = 0;

        //int numLvl2Sector = divRoundUp( numSectors , pointerPerSector );
        //int Lvl2SectorCounter = 0;
        //int numLvl1Sector = divRoundUp( numSectors , (pointerPerSector*pointerPerSector) );
        int realDataSectorCounter = 0; //conut # of data sectors that have been allocated, sectors for locating other sectors are not counted


        memset(dataSectorTable, -1, sizeof(dataSectorTable));//init 
        for(int i=0; i<MaxNumDirectSector; i++){
            // allocate level 1 sector
	        dataSectorTable[i] = freeMap->FindAndSet();
	        ASSERT(dataSectorTable[i] >= 0);
            headerSize += SectorSize;

            char bufx[SectorSize];
            memset(bufx, -1, sizeof(bufx)); // init bufx
            
            for(int offsetx=0; offsetx<SectorSize; offsetx+=sizeof(int)){

                dbglvl2cnt++;
                //if (Lvl2SectorCounter < numLvl2Sector){
                    //Lvl2SectorCounter++;

                    //allocate level 2 sector
                    int tmpx = freeMap->FindAndSet();
                    ASSERT(tmpx >= 0);
                    headerSize += SectorSize;
                    memcpy( (bufx + offsetx) , &tmpx , sizeof(int) );
    

                    char bufy[SectorSize];
                    memset(bufy, -1, sizeof(bufy));
    
                    for(int offsety=0; offsety<SectorSize; offsety+=sizeof(int)){

                        //we only need to allocate new sector if not enough data sectors yet
                        if (realDataSectorCounter < numSectors){
                            realDataSectorCounter++;
                            int tmpy = freeMap->FindAndSet();
                            ASSERT(tmpy >= 0);
                            memcpy( (bufy + offsety) , &tmpy , sizeof(int) );
                        }
                        if (realDataSectorCounter >= numSectors) break;
                    }
                    // write level 2 sector
                    kernel->synchDisk->WriteSector( tmpx , bufy);
                    if (realDataSectorCounter >= numSectors) break;
            
                //}
            }
            // write level 1 sector
            kernel->synchDisk->WriteSector( dataSectorTable[i] , bufx );
            if (realDataSectorCounter >= numSectors) DEBUG(dbgFile, "numLvl1Sector = " << i + 1 << " numlvl2Sector = " << dbglvl2cnt);
            if (realDataSectorCounter >= numSectors) break;
        }

    }
    else { //assume (numSectors <= (pointerPerSector)*(pointerPerSector)*(pointerPerSector)*MaxNumDirectSector), ~128MB
        //triple indirect
        allocateLevel = 3;
        DEBUG(dbgFile, "allocateLevel = " << allocateLevel);

        int dbglvl2cnt = 0;
        int dbglvl3cnt = 0;

        //int numLvl3Sector = divRoundUp( numSectors , pointerPerSector );
        //int numLvl2Sector = divRoundUp( numSectors , (pointerPerSector*pointerPerSector) );
        //int numLvl1Sector = divRoundUp( numSectors , (pointerPerSector*pointerPerSector*pointerPerSector) );
        int realDataSectorCounter = 0; //conut # of data sectors that have been allocated, sectors for locating other sectors are not counted
    
        memset(dataSectorTable, -1, sizeof(dataSectorTable));//init 
        for(int i=0; i<MaxNumDirectSector; i++){
            // allocate level 1 sector
	        dataSectorTable[i] = freeMap->FindAndSet();
	        ASSERT(dataSectorTable[i] >= 0);
            headerSize += SectorSize;

            char bufx[SectorSize];
            memset(bufx, -1, sizeof(bufx)); // init bufx
            
            for(int offsetx=0; offsetx<SectorSize; offsetx+=sizeof(int)){

                    dbglvl2cnt++;

                    //allocate level 2 sector
                    int tmpx = freeMap->FindAndSet();
                    ASSERT(tmpx >= 0);
                    headerSize += SectorSize;
                    memcpy( (bufx + offsetx) , &tmpx , sizeof(int) );
    
                    char bufy[SectorSize];
                    memset(bufy, -1, sizeof(bufy));
    
                    for(int offsety=0; offsety<SectorSize; offsety+=sizeof(int)){

                        dbglvl3cnt++;

                        //allocate level 3 sector
                        int tmpy = freeMap->FindAndSet();
                        ASSERT(tmpy >= 0);
                        headerSize += SectorSize;
                        memcpy( (bufy + offsety) , &tmpy , sizeof(int));

                        char bufz[SectorSize];
                        memset(bufz, -1, sizeof(bufz));

                        for (int offsetz=0; offsetz<SectorSize; offsetz+=sizeof(int)){
                            //we only need to allocate new sector if not enough data sectors yet
                            if (realDataSectorCounter < numSectors){
                                realDataSectorCounter++;
                                int tmpz = freeMap->FindAndSet();
                                ASSERT(tmpz >= 0);
                                memcpy( (bufz + offsetz) , &tmpz , sizeof(int) );
                            }
                            if (realDataSectorCounter >= numSectors) break;
                        }
                        //write level 3 sector
                        kernel->synchDisk->WriteSector( tmpy , bufz );
                        if (realDataSectorCounter >= numSectors) break;
                    }
                    // write level 2 sector
                    kernel->synchDisk->WriteSector( tmpx , bufy);
                    if (realDataSectorCounter >= numSectors) break;
            }
            // write level 1 sector
            kernel->synchDisk->WriteSector( dataSectorTable[i] , bufx );

            if (realDataSectorCounter >= numSectors) DEBUG(dbgFile, "numLvl1Sector=" << i+1 << " numLvl2Sector=" << dbglvl2cnt << " numLvl3Sector=" << dbglvl3cnt);
            if (realDataSectorCounter >= numSectors) break;
        }
   

    }

    /* add by liuchinlin @ mp4 */    
    DEBUG(dbgMP4bonusII, "file header size: " << headerSize/SectorSize << " sectors");
    /* /add by liuchinlin @ mp4 */

	return TRUE;
}
/* /modify by liuchinlin @ mp4 */

//----------------------------------------------------------------------
// FileHeader::Deallocate
// 	De-allocate all the space allocated for data blocks for this file.
//
//	"freeMap" is the bit map of free disk sectors
//----------------------------------------------------------------------

/* modify by liuchinlin @ mp4 */
void FileHeader::Deallocate(PersistentBitmap *freeMap)
{
	// for (int i = 0; i < numSectors; i++)
	// {
	//     ASSERT(freeMap->Test((int)dataSectors[i])); // ought to be marked!
	//     freeMap->Clear((int)dataSectors[i]);
	// }
    
    if (allocateLevel == -1) return; // not allocate yet

    if (allocateLevel == 0){
        for (int i=0; i<MaxNumDirectSector; i++){
            if (dataSectorTable[i] < 0) break; //no more allocated sectors
            //free data sector
            ASSERT(freeMap->Test(dataSectorTable[i]));
            freeMap->Clear(dataSectorTable[i]);
        }
    }
    else if (allocateLevel == 1){
        for (int i=0; i<MaxNumDirectSector; i++){
            if (dataSectorTable[i] < 0) break; //no more allocated sectors
            
            char bufx[SectorSize];
            kernel->synchDisk->ReadSector( dataSectorTable[i] , bufx);
            for (int offsetx=0; offsetx<SectorSize; offsetx+=sizeof(int)){
                int tmpx;
                memcpy( &tmpx, (bufx + offsetx), sizeof(int));
                if (tmpx < 0) break; //no more allocated sectors
                //free data sector
                ASSERT(freeMap->Test(tmpx));
                freeMap->Clear(tmpx);
            }

            // free level 1 sector
            ASSERT(freeMap->Test(dataSectorTable[i]));
            freeMap->Clear(dataSectorTable[i]);
        }

    }
    else if (allocateLevel == 2){
        for (int i=0; i<MaxNumDirectSector; i++){
            if (dataSectorTable[i] < 0) break; //no more allocated sectors
            
            char bufx[SectorSize];
            kernel->synchDisk->ReadSector( dataSectorTable[i] , bufx);
            for (int offsetx=0; offsetx<SectorSize; offsetx+=sizeof(int)){
                int tmpx;
                memcpy( &tmpx, (bufx + offsetx), sizeof(int));
                if (tmpx < 0) break; //no more allocated sectors
                
                char bufy[SectorSize];
                kernel->synchDisk->ReadSector(tmpx, bufy);
                for (int offsety=0; offsety<SectorSize; offsety+=sizeof(int)){
                    int tmpy;
                    memcpy(&tmpy, (bufy + offsety), sizeof(int));
                    if (tmpy < 0) break; // no more allocated sector
                    //free data sector
                    ASSERT(freeMap->Test(tmpy));
                    freeMap->Clear(tmpy);
                }

                //free level 2 sector
                ASSERT(freeMap->Test(tmpx));
                freeMap->Clear(tmpx);
            }

            // free level 1 sector
            ASSERT(freeMap->Test(dataSectorTable[i]));
            freeMap->Clear(dataSectorTable[i]);
        }

    }
    else{ //allocateLevle == 3
        for (int i=0; i<MaxNumDirectSector; i++){
            if (dataSectorTable[i] < 0) break; //no more allocated sectors
            
            char bufx[SectorSize];
            kernel->synchDisk->ReadSector( dataSectorTable[i] , bufx);
            for (int offsetx=0; offsetx<SectorSize; offsetx+=sizeof(int)){
                int tmpx;
                memcpy( &tmpx, (bufx + offsetx), sizeof(int));
                if (tmpx < 0) break; //no more allocated sectors
                
                char bufy[SectorSize];
                kernel->synchDisk->ReadSector(tmpx, bufy);
                for (int offsety=0; offsety<SectorSize; offsety+=sizeof(int)){
                    int tmpy;
                    memcpy(&tmpy, (bufy + offsety), sizeof(int));
                    if (tmpy < 0) break; // no more allocated sector
                    
                    char bufz[SectorSize];
                    kernel->synchDisk->ReadSector(tmpy, bufz);
                    for (int offsetz=0; offsetz<SectorSize; offsetz+=sizeof(int)){
                        int tmpz;
                        memcpy(&tmpz, (bufz + offsetz), sizeof(int));
                        if (tmpz < 0) break; // no more allocated sector
                        //free data sector
                        ASSERT(freeMap->Test(tmpz));
                        freeMap->Clear(tmpz);
                    }
                    
                    //free level 3 sector
                    ASSERT(freeMap->Test(tmpy));
                    freeMap->Clear(tmpy);
                }

                //free level 2 sector
                ASSERT(freeMap->Test(tmpx));
                freeMap->Clear(tmpx);
            }

            // free level 1 sector
            ASSERT(freeMap->Test(dataSectorTable[i]));
            freeMap->Clear(dataSectorTable[i]);
        }

    }

    headerSize = 1*SectorSize;

}
/* /modify by liuchinlin @ mp4 */

//----------------------------------------------------------------------
// FileHeader::FetchFrom
// 	Fetch contents of file header from disk.
//
//	"sector" is the disk sector containing the file header
//----------------------------------------------------------------------

void FileHeader::FetchFrom(int sector)
{
	kernel->synchDisk->ReadSector(sector, (char *)this);

	/*
		MP4 Hint:
		After you add some in-core informations, you will need to rebuild the header's structure
	*/
}

//----------------------------------------------------------------------
// FileHeader::WriteBack
// 	Write the modified contents of the file header back to disk.
//
//	"sector" is the disk sector to contain the file header
//----------------------------------------------------------------------

void FileHeader::WriteBack(int sector)
{
	kernel->synchDisk->WriteSector(sector, (char *)this);

	/*
		MP4 Hint:
		After you add some in-core informations, you may not want to write all fields into disk.
		Use this instead:
		char buf[SectorSize];
		memcpy(buf + offset, &dataToBeWritten, sizeof(dataToBeWritten));
		...
	*/
}

//----------------------------------------------------------------------
// FileHeader::ByteToSector
// 	Return which disk sector is storing a particular byte within the file.
//      This is essentially a translation from a virtual address (the
//	offset in the file) to a physical address (the sector where the
//	data at the offset is stored).
//
//	"offset" is the location within the file of the byte in question
//----------------------------------------------------------------------

/* modify by liuchinlin @ mp4 */
int FileHeader::ByteToSector(int offset)
{
	// return (dataSectors[offset / SectorSize]);

    int pointerPerSector = SectorSize / sizeof(int);
    int dataSectorOrder = offset / SectorSize; // which leaf?

    if (allocateLevel == 0){
        //find leaf.sectorID, = dataSectorTable[dataSectorOrder]
        return dataSectorTable[dataSectorOrder];
    }
    else if (allocateLevel == 1){
        // find leaf.parent.sectorID, = dataSectorTable[lvl1SectorOrder]
        int lvl1SectorOrder = dataSectorOrder / pointerPerSector;


        //find leaf.sectorID, = tmpx
        char bufx[SectorSize];
        kernel->synchDisk->ReadSector( dataSectorTable[lvl1SectorOrder] , bufx );

        int offsetx = (dataSectorOrder - (lvl1SectorOrder * pointerPerSector) )*sizeof(int) ;
        int tmpx;
        memcpy(&tmpx, (bufx + offsetx), sizeof(int));
        
        return tmpx;     
    }
    else if (allocateLevel == 2){
        
        // find leaf.parent.parent.sectorID, = dataSectorTable[lvl1SectorOrder]
        int lvl1SectorOrder = dataSectorOrder / (pointerPerSector*pointerPerSector);
        
        //find leaf.parent.sectorID, = tmpx
        char bufx[SectorSize];
        kernel->synchDisk->ReadSector( dataSectorTable[lvl1SectorOrder] , bufx );
        
        int lvl2SectorOrder = dataSectorOrder / pointerPerSector;
        int offsetx = (lvl2SectorOrder - (lvl1SectorOrder * pointerPerSector))*sizeof(int);
        int tmpx;
        memcpy(&tmpx, (bufx + offsetx), sizeof(int));
        
        //find leaf.sectorID, = tmpy
        char bufy[SectorSize];
        kernel->synchDisk->ReadSector( tmpx, bufy );

        int offsety = (dataSectorOrder - (lvl2SectorOrder * pointerPerSector))*sizeof(int);
        int tmpy;
        memcpy(&tmpy, (bufy + offsety), sizeof(int));

        return tmpy;     
    }
    else { //allocateLevel == 3

        //find leaf.parent.parent.parent.sectorID, = dataSectorTable[lvl1SectorOrder]
        int lvl1SectorOrder = dataSectorOrder / (pointerPerSector*pointerPerSector*pointerPerSector);

        //find leaf.parent.parent.sectorID, = tmpx
        char bufx[SectorSize];
        kernel->synchDisk->ReadSector( dataSectorTable[lvl1SectorOrder] , bufx );
        
        int lvl2SectorOrder = dataSectorOrder / (pointerPerSector*pointerPerSector);
        int offsetx = (lvl2SectorOrder - (lvl1SectorOrder * pointerPerSector))*sizeof(int);
        int tmpx;
        memcpy(&tmpx, (bufx + offsetx), sizeof(int));

        //find leaf.parent.sectorID, = tmpy
        char bufy[SectorSize];
        kernel->synchDisk->ReadSector( tmpx, bufy );

        int lvl3SectorOrder = dataSectorOrder / pointerPerSector;
        int offsety = (lvl3SectorOrder - (lvl2SectorOrder * pointerPerSector))*sizeof(int);
        int tmpy;
        memcpy(&tmpy, (bufy + offsety), sizeof(int));

        //find leaf.sectorID, = tmpz
        char bufz[SectorSize];
        kernel->synchDisk->ReadSector( tmpy, bufz);

        int offsetz = (dataSectorOrder - (lvl3SectorOrder * pointerPerSector))*sizeof(int);
        int tmpz;
        memcpy(&tmpz , (bufz + offsetz), sizeof(int));

        return tmpz;
    }

}
/* /modify by liuchinlin @ mp4 */

//----------------------------------------------------------------------
// FileHeader::FileLength
// 	Return the number of bytes in the file.
//----------------------------------------------------------------------

int FileHeader::FileLength()
{
	return numBytes;
}

//----------------------------------------------------------------------
// FileHeader::Print
// 	Print the contents of the file header, and the contents of all
//	the data blocks pointed to by the file header.
//----------------------------------------------------------------------

void FileHeader::Print()
{
    /* comment out by liuchinlin @ mp4 */
	// int i, j, k;
	// char *data = new char[SectorSize];

	// printf("FileHeader contents.  File size: %d.  File blocks:\n", numBytes);
	// for (i = 0; i < numSectors; i++)
	//     printf("%d ", dataSectors[i]);
	// printf("\nFile contents:\n");
	// for (i = k = 0; i < numSectors; i++)
	// {
	//     kernel->synchDisk->ReadSector(dataSectors[i], data);
	//     for (j = 0; (j < SectorSize) && (k < numBytes); j++, k++)
	//     {
	//         if ('\040' <= data[j] && data[j] <= '\176') // isprint(data[j])
	//             printf("%c", data[j]);
	//         else
	//             printf("\\%x", (unsigned char)data[j]);
	//     }
	//     printf("\n");
	// }
	// delete[] data;
    /* /comment out by liuchinlin @ mp4 */
}
